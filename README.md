# Progetto: dal DICOM all'AR
Il progetto mira ad ottenere una controparte di DICOM nella realtà aumentata.
È stata sviluppata una versione base del prototipo che permette di manipolare in realtà aumentata contenuti DICOM nelle 2 forme di:

* modello tridimensionale ottenuto da dati DICOM mediante segmentazione.
* Slice bidimensionali del modello 3D.

Il prototipo è stato sviluppato con Unity e il deploy è stato effettuato su:

*	Tablet Android
*	Headset Meta 2

Per quanto riguarda Android si è utilizzato l’SDK Google Arcore, per Meta 2 l’SDK omonimo.
È stata poi sviluppata una 2° versione del prototipo che è un’estensione delle funzionalità iniziali. Consente di visualizzare in realtà aumentata l’integrazione delle 2 rappresentazioni bidimensionale e tridimensionale nello stesso sistema di riferimento, ovvero quello detto “del paziente” e codificato all’interno dei file DICOM.


## Breve guida all’utilizzo

La catena tecnologica mediante la quale ottenere il modello tridimensionale è brevemente spiegata nel seguito. Per quanto riguarda l’importazione delle slices bidimensionali è stato sviluppato un modulo di classi in C#, che fungono da supporto agli script associati ai gameobject di Unity.

### Scripts matlab
Il risultato della segmentazione, ovvero dell’estrazione delle zone di interesse dal volume generato dallo stack di immagini DICOM, ci è stato fornito da un team di ingegneri biomedici, con cui abbiamo collaborato nello sviluppo del progetto.
Si è fatto uso di matlab al fine di convertire le mesh da loro fornite (in un file matlab .mat) in un formato compatibile con Unity(.fbx).
Contemporaneamente, è stato ottenuto uno altro modello tridimensionale mediante un procedimento indipendente, i cui dettagli sono indicati nei paragrafi seguenti, per poter valutare il processo di conversione dal DICOM alla AR in maniera completa, cioè a partire dai dataset originale.
Gli script matlab producono in output un .obj che può essere importato in Blender al fine di:

* centrare il pivot: il pivot point è il centro matematico per la rotazione e la traslazione, centrarlo permette di ottenere una manipolazione più intuitiva durante l'esperienza aumentata (ciò è necessario solo nel caso in cui non si voglia mantenere il modello nel sistema di riferimento del paziente – 1° versione del prototipo)
* ottenere un modello .fbx con le informazioni sul colore delle facce (solo nel caso la mesh contenga anche informazioni sulle intensità delle facce).

Prima di utilizzare gli script è necessario includere nella search path di matlab i percorsi alle seguenti cartelle: WOBJ_toolbox_Version2b e stlwrite, situate in Matlab/FileExchange/ rispetto alla root del repository (rinominando la funzione stlwrite in stlwrite2 per evitare problemi di omonimie).
Inoltre, sono forniti:

*	Script per la visualizzazione dei risultati ottenuti e per lo studio di future tecniche di visualizzazione tridimensionale.
*	Script che contiene la logica di interpolazione, dal momento che rappresenterà una delle direzioni future.

Altre indicazioni sono nei commenti al codice matlab.

### Blender, rapida guida per esportare da .obj a .fbx con Blender 2.80
Blender è stato sfruttato per:

*	Ridefinire il pivot point del modello
*	Convertire la mesh a .fbx (formato compatibile con Unity)

Per ridefinire il pivot point e convertire a .fbx è necessario:

* rimuovere tutti gli oggetti dalla scena di Blender.
* importare il modello in .obj: file / import / Wavefront
* centrare il pivot point: tasto dx sul modello / “geometry to origin”
* esportare: file / Export / FBX.

Nella 2° versione del prototipo è necessario mantenere il modello nel sistema di riferimento del paziente, bisogna quindi mantenere le sue dimensioni coerenti con la realtà, impostando “scale” a 0.0001, dal menù di esportazione sulla sinistra dell’editor.
Nell’ottica di automatizzare il processo di importazione, è fornito uno script in python (sfruttando le API che Blender espone) che centra il pivot ed esegue la conversione al formato .fbx, risparmiando di agire manualmente dall’editor di Blender.

### Slicer, rapida guida per la segmentazione con Slicer 4.10
Il procedimento autonomo per eseguire la segmentazione è stato realizzato tramite il software open source Slicer.
Per ottenere una mesh codificata in .obj a partire da un cartella contenente i file DICOM relativi ad una serie è necessario:

* caricare la serie DICOM: file / DICOM / import / selezionare la cartella / load.
* aprire il segment editor: Dalla barra degli strumenti, nella sezione "Modules", selezionare dal dropdown: “Segment editor“
* aggiungere segmentazione: dal "segment editor" premere su "Add". Nel caso non fosse presente l’icona di un occhio accanto al segmento appena aggiunto, fare click sulla cella che presenta l’icona dell’occhio nella sua intestazione.
* segmentare: scorrere in basso, nella sezione "Effects" premere "threshold" e regolare il “threshold range” visualizzando sulla dx le zone estratte con l’intervallo selezionato. Ottenuto un risultato soddisfacente procedere con “Apply”. Per poter visualizzare il segmento ottenuto è necessario fare click sul pulsante “Show3D”, posto immediatamente sopra alla tabella dei segmenti.
* rifinire la segmentazione: sempre nella sezione "Modules" è disponibile una serie di strumenti con i quali rimuovere il rumore prodotto dalla sogliatura. Nel riquadro in alto a destra è possibile non solo visualizzare, ma anche modificare il modello mediante lo strumento “scissors”. Può essere utile agire sulle singole slices e sulle ricostruzioni corrispondenti ai piani ortogonali, applicando gli strumenti su ciascuna di esse, scorrendole con rotellina del mouse.
* esportare il risultato ottenuto: sopra all’elenco dei segmenti premere sulla freccia a dx di “Segmentations”, poi “Export to files…”. Come formato di esportazione, si può scegliere fra stl e obj. Nel caso sia necessario il colore scelto precedentemente, è conveniente selezionare .obj in quanto .stl non fornisce il supporto per il colore associato alle facce (.stl definisce solo la geometria del modello). Per ottenere un modello nello stesso sistema di riferimento specificato da DICOM (detto “del paziente”) bisogna specificare “Scale Size: 1.000” e “Coordinate system: LPS”. Ciò è necessario al fine di integrare la rappresentazione bidimensionale con quella tridimensionale nello stesso sistema di riferimento (2° versione del prototipo).

Esistono, in Slicer, alternative al procedimento descritto, ma la soluzione proposta è indicata dalla documentazione come quella di più recente introduzione.

### Unity
Il prototipo è stato sviluppato con Unity 2018.4.14f1.
Sono state predisposte diverse scene:

*	Visualizzazione/manipolazione su Arcore di modello e slices separatamente (rispettivamente: ARBaseVersionModel e ARBaseVersionSlices)
*	Visualizzazione/manipolazione su Meta 2 di modello (MetaModel)
*	Integrazione di modello tridimensionale e bidimensionale sullo stesso sistema di riferimento su Arcore (ARExtendedMeshCut).

I prefab nella cartella Assets/Resources/Prefabs/Models contengono le mesh ottenute a seguito del procedimento descritto precedentemente.
È stato implementato un modulo di classi per estrarre i pixel dal file DICOM, elaborarli e disporre, dimensionare ed orientare le slices nello spazio secondo le convenzioni definite dallo standard nelle sezioni  Image Pixel Module, ImagePlaneModule, Modality LUT Module, VOI LUT Module.
Per quanto riguarda la gestione delle immagini estratte dallo stack DICOM di partenza, il progetto contiene 2 scene di esempio nella cartella Assets/Scenes/DesktopExamples che hanno lo scopo di mostrare come utilizzare le classi relative al caricamento delle immagini DICOM e alla gestione dell’intersezione della slice bidimensionale scelta dall’utente con il modello tridimensionale. Tali scene non necessitano di eseguire il deploy sui dispositivi e sono pensate per l’esecuzione all’interno dell’editor. Per comprenderne il funzionamento, è necessario osservare la scena attraverso la scene view e modificare il valore dello slider dall’inspector. Maggiori indicazioni si trovano nel codice degli script.


### Preparazione dell’ambiente
Prima di effettuare il deploy sul tablet Android è necessario copiare la cartella DICOM (contenuta in Assets/Resources) all’interno del dispositivo di destinazione, al percorso: Android/data/com.Uni.DicomAR/files, (corrispondente al percorso storage/emulated/0/Android/data/com.Uni.DicomAR/files).
