%{
script for generating an obj mesh from .mat without array of intensities(Vq)
%}

%path to the .mat file containing the mesh
MESHPATH = 'D:\Daniele\Unibo2\Augmented_Reality\Kidney\paz2_final.mat'; %MESHPATH = 'D:\Daniele\Unibo2\Augmented_Reality\Heart\Heart.mat';
OUTPUT_FILENAME = 'kidney_single_noVq_original.obj';

load(MESHPATH);

%create single material as the .mat doesn't contain color intensities info
numEntry=1;
material(numEntry).type='newmtl';
material(numEntry).data='mat-1';
numEntry=numEntry+1;
material(numEntry).type='Ka';
material(numEntry).data=[0.5 0.5 0.5]; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Kd';
material(numEntry).data=[0.5 0.5 0.5]; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Ks';
material(numEntry).data=[1 1 1];
numEntry=numEntry+1;
material(numEntry).type='illum';
material(numEntry).data=2;
numEntry=numEntry+1;
material(numEntry).type='Ns';
material(numEntry).data=27;

%general info shared by all faces
OBJ.vertices = endmesh.vertices;
%OBJ.vertices_normal = N;
OBJ.material = material;

%write every face with the associated material
%OBJ.objects = zeros(1, length(faces));  %preallocated for performance issue
numEntry=1;
for i = 1:length(endmesh.faces)
   OBJ.objects(numEntry).type='usemtl';
   OBJ.objects(numEntry).data='mat-1';  
   
   %fprintf('String: %s\n', OBJ.objects(numEntry).data);

   numEntry=numEntry+1;
   
   OBJ.objects(numEntry).type='f';
   OBJ.objects(numEntry).data.vertices=endmesh.faces(i,:);  %this syntax means take the ith row
   OBJ.objects(numEntry).data.normal=endmesh.faces(i, :);
   
   numEntry=numEntry+1;
end

%fprintf('%i\n', fac(1));
%patch('Faces',fac,'Vertices',ver);

write_wobj(OBJ,OUTPUT_FILENAME);
