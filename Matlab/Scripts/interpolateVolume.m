%this is the interpolation script which interpolates data to make a
%isotropic volume (i. e. a volume with voxels of equal height, width and length)
%encoded in a .raw file.

%after having encoded the pixel data in a .raw file (made of uint8 values)
%with a image processing software like fiji/imagej, this scripts outputs another .raw file
%containing the voxels values of the same volume, but isotropic.
%Uppercase comments points the element of the dicom files that must be
%edited before running this script. They should be extracted from dicom file
%by using a normal dicom processing file library.

PATH_TO_RAW_FILE = 'C:\Users\Patrizia\Desktop\sitkkid.raw';
OUTPUT_RAW_FILE = 'kidMatlab.raw';

fileID = fopen(PATH_TO_RAW_FILE);
MRVolumeA  = fread(fileID); %default interpretation : uint8
D=reshape(MRVolumeA,[224, 224, 32]); %ROWS, COLUMNS, number of slices of the set of the dicom slices

D=double(squeeze(D));       % Remove singleton dimension and convert from int to double
szD_a=size(D);              % calculate size of original image stack
vox_a = [1.45,1.45,5.5];    % define original voxel size(PIXELSPACINGX, PIXELSPACINGY, SPACINGBETWEENSLICES)
vox_b = [1,1,1];           % set target voxel size
szD_b=ceil((szD_a-1).*vox_a./vox_b)+1; % compute size of target image stack
                                       % plus and minus 1 account for 
                                       % pixel (1,1,1) being at coordinate [0,0,0]
                                       
% Get x,y,z coordinates of each voxel in original image stack
[Xa,Ya,Za]=meshgrid(...
    [0:szD_a(1)-1]*vox_a(1),...
    [0:szD_a(2)-1]*vox_a(2),...
    [0:szD_a(3)-1]*vox_a(3));

% Define x,y,z coordinates of each voxel in target image stack
[Xb,Yb,Zb]=meshgrid(...
    [0:szD_b(1)-1]*vox_b(1),...
    [0:szD_b(2)-1]*vox_b(2),...
    [0:szD_b(3)-1]*vox_b(3));

% interpolate imagestack from original to target coordinates
D_target=interp3(Xa,Ya,Za,double(D),Xb,Yb,Zb);
volshow(D_target);
fprintf('new size: %d, %d, %d',  szD_b(1), szD_b(2), szD_b(3));

fileID = fopen(OUTPUT_RAW_FILE,'w');
fwrite(fileID,D_target,'uint8');
fclose(fileID);
