%{
This script displays the volume extracted from the folder of DICOM files, 
by the means of a volumetric tecnique called "volume rendering".
The volume is correctly scaled by performing a step of interpolation before 
rendering.
%}

%fullfile wants the path to folder containing DICOM files
DICOM_DIR = fullfile('D:\', 'Daniele', 'Unibo2', 'Augmented_Reality', 'Kidney', 'DICOM', 'Series_401_T2W_SPIR_RT')

sourcetable = dicomCollection(DICOM_DIR);
V = dicomreadVolume(sourcetable,'s1','MakeIsotropic',true);
V = squeeze(V);
intensity = [0 20 40 120 220 1024];
alpha = [0 0 0.15 0.3 0.38 0.5];
color = ([0 0 0; 43 0 0; 103 37 20; 199 155 97; 216 213 201; 255 255 255])/255;
queryPoints = linspace(min(intensity),max(intensity),256);
alphamap = interp1(intensity,alpha,queryPoints)';
colormap = interp1(intensity,color,queryPoints);
ViewPnl = uipanel(figure,'Position',[0 0 1 1],'Title','Isotropic 4-D Dicom Volume');
volshow(V,'Colormap',colormap,'Alphamap',alphamap,'CameraPosition',[3 3 4],'Parent',ViewPnl);


