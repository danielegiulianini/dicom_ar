function [numEntry] = createMaterial(name,Ka, Kd, Ks, illum, Ns)
%createMaterial this function creates a material encoded as defined in obj
%standard
material(numEntry).type='newmtl';
material(numEntry).data=name;
numEntry=numEntry+1;
material(numEntry).type='Ka';
material(numEntry).data=Ka; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Kd';
material(numEntry).data=Kd; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Ks';
material(numEntry).data=Ks;
numEntry=numEntry+1;
material(numEntry).type='illum';
material(numEntry).data=illum;
numEntry=numEntry+1;
material(numEntry).type='Ns';
material(numEntry).data=Ns;
end

