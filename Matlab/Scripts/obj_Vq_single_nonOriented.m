%{
script finale che genera una mesh codificata in obj che appare come la visualizza 
matlab tramite la funzione patch sfruttando cio� la codifica rgb impiegata da 
matlab (contenuta nella matrice cmap).
%}

%path to the .mat file containing the mesh
MESHPATH = 'D:\Daniele\Unibo2\Augmented_Reality\mesh_000.mat';  %MESHPATH ='C:\Users\Patrizia\Downloads\000_aperta.mat';
OUTPUT_FILENAME = 'atriumNewCOlors12.obj';

load(MESHPATH); 

% 1. creazione di tutti i 255 materials
numEntry=1;
for i = 1:size(cmap, 1)
    material(numEntry).type='newmtl';
    material(numEntry).data=strcat('mat-', num2str(i));
    numEntry=numEntry+1;
    material(numEntry).type='Ka';
    material(numEntry).data=[cmap(i, 1) cmap(i,2) cmap(i, 3)]; % rgb normalized to 1
    numEntry=numEntry+1;
    material(numEntry).type='Kd';
    material(numEntry).data=[cmap(i, 1) cmap(i,2) cmap(i, 3)]; % rgb normalized to 1
    numEntry=numEntry+1;
    material(numEntry).type='Ks';
    material(numEntry).data=[1 1 1];
    numEntry=numEntry+1;
    material(numEntry).type='illum';
    material(numEntry).data=2;
    numEntry=numEntry+1;
    material(numEntry).type='Ns';
    material(numEntry).data=27;
    numEntry=numEntry+1;
end

maxIntensity = max(Vq);
minIntensity = min(Vq); %oppure 0

%2. mapping alla riga della matrice di palette tramite la formula:
%   round((( Vq(i)-minIntensity )/(maxIntensity - minIntensity))*255, 1)

for i = 1:length(Vq)
    %fprintf('array index:%d\n', round((( Vq(i)-minIntensity )/(maxIntensity - minIntensity))*255)+1   );
    intensities(i) = round((( Vq(i)-minIntensity )/(maxIntensity - minIntensity))*255) +1;  %rounding to limit number of materials
end

%general info shared by all faces
OBJ.vertices = vertex;
%OBJ.vertices_normal = N;
OBJ.material = material;

%write every face with the associated material
%OBJ.objects = zeros(1, length(faces));  %preallocated for performance issue
numEntry=1;
for i = 1:length(faces)
   OBJ.objects(numEntry).type='usemtl';
   OBJ.objects(numEntry).data=strcat('mat-', num2str(intensities(i)));   %access to the color (normalized)value of the face
   numEntry=numEntry+1;
   
   OBJ.objects(numEntry).type='f';
   OBJ.objects(numEntry).data.vertices=faces(i,:);  %this syntax means take the ith row
   OBJ.objects(numEntry).data.normal=faces(i, :);
   
   numEntry=numEntry+1;
end

%fprintf('%i\n', fac(1));
%patch('Faces',fac,'Vertices',ver);

write_wobj(OBJ,OUTPUT_FILENAME);