%{
script per estrarre tante mesh contenute in un singolo file .mat (es Heart.mat)
al fine di creare delle animazioni della struttura anatomica(in dinamica). 
Esiste un plugin per Blender (stopmotion obj) che permette di generare un'animazione
(obj sequence) a partire da un'insieme di mesh obj. Una volta importata in 
Blender, si pu� notare visivamente l'evoluzione temporale della struttura
anatomica. Tuttavia, ad oggi, tramite Blender, le obj sequence non possono 
essere esportate come animazioni fbx e per questo non sono importabili in
Unity.
%}

%path to the .mat file containing the mesh
MESH_PATH = 'D:\Daniele\Unibo2\Augmented_Reality\Heart\Heart.mat';  %MESH_PATH= 'D:\Daniele\Unibo2\Augmented_Reality\Heart\1112722.mat';

load(MESH_PATH);

%create a single material because there aren't info on intensities.
numEntry=1;
material(numEntry).type='newmtl';
material(numEntry).data='mat-1';
numEntry=numEntry+1;
material(numEntry).type='Ka';
material(numEntry).data=[0.5 0.5 0.5]; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Kd';
material(numEntry).data=[0.5 0.5 0.5]; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Ks';
material(numEntry).data=[1 1 1];
numEntry=numEntry+1;
material(numEntry).type='illum';
material(numEntry).data=2;
numEntry=numEntry+1;
material(numEntry).type='Ns';
material(numEntry).data=27;


for i = 1:length(endmesh) 
    %general info shared by all faces
    OBJ.vertices = endmesh(i).vertices;
    %OBJ.vertices_normal = N;
    OBJ.material = material;

    %write every face with the associated material
    numEntry=1;
    for j = 1:length(endmesh(i).faces)
       OBJ.objects(numEntry).type='usemtl';
       OBJ.objects(numEntry).data='mat-1';
       numEntry=numEntry+1;

       OBJ.objects(numEntry).type='f';
       OBJ.objects(numEntry).data.vertices=endmesh(i).faces(j,:);  %this syntax means take the ith row
       OBJ.objects(numEntry).data.normal=endmesh(i).faces(j, :);

       numEntry=numEntry+1;
    end

    %the following convention is needed for the python script implied for 
    %generating the animation (stop-motion):
    %if less than 10 -> number must be followed by 2 zeroes
    %if less than than 100 -> number must be followed by 1 zero
    if i<10
        objName =  strcat('heart_obj_00', num2str(i), '.obj');
    else 
        objName =  strcat('heart_obj_0', num2str(i), '.obj');
    end 

    write_wobj(OBJ, objName);
    
    OBJ.vertices = [];   %clearing because meshes coming after bigger ones could inherit wrong references (if a mesh is followed by a mesh with more vertices)
    OBJ.objects = [];
end