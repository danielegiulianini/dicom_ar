%{
This script translates the mesh inside paz2_final.mat from the 
coordinate systems used in that file to the DICOM patient coordinates
system (RCS), by extracting the corresponding matrix from the DICOM file of the 
1st slice of the corresponding series. The mesh was already correctly
scaled, only translating and rotating was missing. This produces a .stl
file.
%}

%path to the .mat file containing the mesh
MESHDIR = 'D:\Daniele\Unibo2\Augmented_Reality\Kidney\paz2_final.mat';
%path to the folder containing the dicom files
DICOMDIR = 'D:\Daniele\Unibo2\Augmented_Reality\Kidney\DICOM\Series_301_T2 SPAIR cor bh\';

load(MESHDIR);

%1. leggo il primo DICOM della serie per reperire la rispettiva matrice affine 
%(scala e rotazione sono sempre le stesse da slice a slice, la traslazione cambia)
fullFileName = fullfile(DICOMDIR, 'NNI20');
infodicom = dicominfo(fullFileName);

[Mtf,Rtf] = ExtractMatrixNoScale(infodicom);

dim = size(endmesh.vertices);

myVertices=zeros(dim(1), dim(2));

for i = 1:dim(1)
    vector4d = [endmesh.vertices(i, :, :) 1];
    transformedVector = Mtf*vector4d';  %apply affine matrix
    myVertices(i, :, :) = transformedVector(1:3);
end

%patch('Faces', endmesh.faces, 'Vertices', myVertices,'EdgeColor', 'none');
            
stlwrite2('kidneyNoScale.stl', endmesh.faces, myVertices);



