%{
This script translates the mesh inside paz2_final.mat from the 
coordinate systems used in that file to the DICOM patient coordinates
system (RCS), by extracting the corresponding matrix from the DICOM file of the 
1st slice of the corresponding series. The mesh was already correctly
scaled, only translating and rotating was missing.
%}

%path to the .mat file containing the mesh
MESHDIR = 'D:\Daniele\Unibo2\Augmented_Reality\Kidney\paz2_final.mat';
%path to the folder containing the dicom files
DICOMDIR = 'D:\Daniele\Unibo2\Augmented_Reality\Kidney\DICOM\Series_301_T2 SPAIR cor bh\';
OUTPUT_FILENAME= 'kidneyNoScale.obj';

%create single material as the .mat doesn't contain color intensities info
numEntry=1;
material(numEntry).type='newmtl';
material(numEntry).data='mat-1';
numEntry=numEntry+1;
material(numEntry).type='Ka';
material(numEntry).data=[0.5 0.5 0.5]; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Kd';
material(numEntry).data=[0.5 0.5 0.5]; % rgb normalized to 1
numEntry=numEntry+1;
material(numEntry).type='Ks';
material(numEntry).data=[1 1 1];
numEntry=numEntry+1;
material(numEntry).type='illum';
material(numEntry).data=2;
numEntry=numEntry+1;
material(numEntry).type='Ns';
material(numEntry).data=27;

load(MESHDIR);

fullFileName = fullfile(DICOMDIR, 'NNI20'); %take first slice for imagePositionPatient
infodicom = dicominfo(fullFileName);

[Mtf,Rtf] = ExtractMatrixNoScale(infodicom); %get the affine matrix of the frist slice from DICOM header

dim = size(endmesh.vertices);

myVertices=zeros(dim(1), dim(2));   %declare array of vertices

for i = 1:size(endmesh.vertices, 1)
    vector4d = [endmesh.vertices(i, :, :) 1];
    transformedVector = Mtf*vector4d';  %apply affine matrix
    myVertices(i, :, :) = transformedVector(1:3);
end

%general info shared by all faces
OBJ.vertices = myVertices;
%OBJ.vertices_normal = N;
OBJ.material = material;

patch('Faces', endmesh.faces, 'Vertices', myVertices,'EdgeColor', 'none');

write_wobj(OBJ,OUTPUT_FILENAME);
