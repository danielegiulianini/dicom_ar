%this script displays the mesh without colors

%path to the .mat file containing the mesh
MESHPATH = 'D:\Daniele\Unibo2\Augmented_Reality\Kidney\paz2_final.mat'; %MESHPATH = 'D:\Daniele\Unibo2\Augmented_Reality\Heart\1112722.mat';
load(MESHPATH);  %load();
patch('Faces', endmesh.faces, 'Vertices', endmesh.vertices,'EdgeColor', 'none');