﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provided a mapping between input pixels and output, this class performs a transformation from ushort to 
/// ushort by the means of a LUT for efficiency reasons. 
/// </summary>
public class LUTIntTrasformation : IImageTransformation<ushort>
{
    private AImage<ushort> lut;
    private AImage<ushort> input;
    private AImage<ushort> output;

    //I should implement the constructor with the LUT already made, extracted from DICOM

    public LUTIntTrasformation(AImage<ushort> input, PixelFunction<ushort, ushort> pf)
    {
        this.input = input;//deep copy of input image? not necessary as I don't edit it.
        lut = new AImage<ushort>(32768, 2);
        output = new AImage<ushort>(input.Width, input.Height);
        for (ushort i = 0; i < 65535; i++) //I can reduce the range to the min / max value of the image
        {
            ushort ris = pf(i);
            lut[i] = ris>65535 ?(ushort) 65535 : ris;   //should check for underflow too (under 0)
        }
    }

    public AImage<ushort> Apply()
    {
        for (int i = 0; i < output.Width * output.Height; i++)
        {
            output[i] = lut[input[i]];
        }
        return output;
    }
}
