﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/// <summary>
/// This class computes the affine matrix for translating from images coordinates system to "patient
/// coordinates system", the RCS used by DICOM, by reading and interpreting DICOM tags from the DICOM file.
/// </summary>
public class SpatialInfoExtractor
{
    private DICOMDataset dds;

    private static readonly float customRatio = 2.5f;   //2.5 to make it more user-appealing

    public SpatialInfoExtractor(DICOMDataset dds)
    {
        this.dds = dds;
    }

    public Transformation3D GetTransformation()
    {
        Debug.Log("dds");

        var R = CreateRotationMatrix();
        AMatrix4x4 DICOMRotationMatrix = new AMatrix4x4(R);

        var x = (double)dds.FindFirst(ImageProcessingTagHelper.Image​Position​Patient()).GetMultipleData()[0];
        var y = (double)dds.FindFirst(ImageProcessingTagHelper.Image​Position​Patient()).GetMultipleData()[1];
        var z = (double)dds.FindFirst(ImageProcessingTagHelper.Image​Position​Patient()).GetMultipleData()[2];

        var pixelSpacingX = (double)(dds.FindFirst(ImageProcessingTagHelper.PixelSpacing()).GetMultipleData()[0]);
        var pixelSpacingY = (double)(dds.FindFirst(ImageProcessingTagHelper.PixelSpacing()).GetMultipleData()[1]);

        //these 2 fields are not strictly necessary for the frame to be displayed but maybe will be useful in future.
        var rows = (ushort)dds.FindFirst(ImageProcessingTagHelper.Rows()).GetData();
        var columns = (ushort)dds.FindFirst(ImageProcessingTagHelper.Rows()).GetData();


        float fromMetersToMillimeters = 1.0f / 1000.0f * customRatio; //1000 as mm = 1 mt / 1000 and unity unit is 1 mt, 
        x = x * fromMetersToMillimeters;
        y = y * fromMetersToMillimeters;
        z = z * fromMetersToMillimeters;


        var t = new Transformation3D(new Vector3((float)x, (float)z, (float)y),
            MatricesUtility.FromRadiansToDegrees(MatricesUtility.GetAngles2(DICOMRotationMatrix)),
            new Vector3((float)pixelSpacingX * fromMetersToMillimeters, (float)pixelSpacingY * fromMetersToMillimeters, 1),
            new Vector2(rows, columns)); 

        /*
         * The following part of code computes the centroid coordinates.
         * Knowing centroid coordinates is required for Meta 2, as sprites doesn't show well with the headset. 
         * Instead of sprites, a primitive object type provided by Unity (quad) must be used, because meshrenderer 
         * doesn't suffer the sprite renderer problem of transparency. The other way is to solve the transparency 
         * issue with a new shader, that behaves differently from the standard shader provided by Unity and 
         * attached by default to objects.
         * However, quads with standard shader can be seen only from one side, and are invisible by the other;
         * to solve this issue a double sided shader is required and must be be implemented in the future.
         * Quads have their pivot point in their centroid point while for sprites it 
         * could be set by the programmer.
         * The idea is to:
         *  - get the centroid point of the slice (coordinates: rows/2, columns/2, 0)
         *  - compute the affine matrix from the DICOM fields for the specific slice
         *  - apply the matrix to the starting coordinates
         * */

        var Mtf = CreateAffineMatrix();
        var centroid = new double[] { rows / 2, columns / 2, 0d, 1d };
        var mypoint = new double[,] { { centroid[0] }, { centroid[1] }, { centroid[2] }, { centroid[3] } };//this is the encapsulation needed for MultiplyMatrix

        var point4 = MatricesUtility.MultiplyMatrix(Mtf, mypoint);
        var homogeneusCentroid = MatricesUtility.GetColumn(point4, 0);

        //Debug.Log("centroide prima di conversione a mm");
        //DebugPrintArray4(MatricesUtility.GetColumn(point4, 0));

        homogeneusCentroid[0] *= fromMetersToMillimeters;
        homogeneusCentroid[1] *= fromMetersToMillimeters;
        homogeneusCentroid[2] *= fromMetersToMillimeters;

        //Debug.Log("centroide dopo conversione a mm");
        //DebugPrintVector3(new Vector3((float)homogeneusCentroid[0], (float)homogeneusCentroid[1], (float)homogeneusCentroid[2]));

        t.CentroidPosition = new Vector3((float)homogeneusCentroid[0], (float)homogeneusCentroid[2], (float)homogeneusCentroid[1]); //swapp z and y 
        t.CentroidScale = new Vector3((float)(rows * pixelSpacingX * fromMetersToMillimeters), (float)(columns * pixelSpacingY * fromMetersToMillimeters), 1f);
        return t;
    }

    static void DebugPrintArray4(double[] arr)
    {
        Debug.Log("vector: " + arr[0] + ",  " + arr[1] + ", " + arr[2] + ", " + arr[3]);
    }

    static void DebugPrintVector3(Vector3 v)
    {
        Debug.Log(v.ToString("F4"));
    }

    static void DebugPrintMatrix44(double[,] arr)
    {
        Debug.Log("matrix : -----------------------------------------------------------");
        int rowLength = arr.GetLength(0);
        int colLength = arr.GetLength(1);

        for (int i = 0; i < rowLength; i++)
        {
            for (int j = 0; j < colLength; j++)
            {
                Debug.Log(string.Format("{0} ", arr[i, j]));
            }
            Debug.Log(Environment.NewLine + Environment.NewLine);
        }
    }

    private double[,] CreateRotationMatrix()
    {
        var ImageOrientationList = dds.FindFirst(ImageProcessingTagHelper.ImageOrientation​Patient()).GetMultipleData();

        var orientationRows = new double[] { (double)ImageOrientationList[0], (double)ImageOrientationList[1], (double)ImageOrientationList[2] };
        var orientationColumns = new double[] { (double)ImageOrientationList[3], (double)ImageOrientationList[4], (double)ImageOrientationList[5] };
        double[] crossProduct = MatricesUtility.Cross3D(orientationRows, orientationColumns);

        var R = new double[,] { { orientationRows[0], orientationColumns[0], crossProduct[0], 0d }, { orientationRows[1], orientationColumns[1], crossProduct[1], 0d }, { orientationRows[2], orientationColumns[2], crossProduct[2], 0d }, { 0d, 0d, 0d, 1d } };
        return R;
    }

    public double[,] CreateAffineMatrix()
    {
        var R = CreateRotationMatrix();

        var x = (double)dds.FindFirst(ImageProcessingTagHelper.Image​Position​Patient()).GetMultipleData()[0];
        var y = (double)dds.FindFirst(ImageProcessingTagHelper.Image​Position​Patient()).GetMultipleData()[1];
        var z = (double)dds.FindFirst(ImageProcessingTagHelper.Image​Position​Patient()).GetMultipleData()[2];

        var pixelSpacingX = (double)(dds.FindFirst(ImageProcessingTagHelper.PixelSpacing()).GetMultipleData()[0]);
        var pixelSpacingY = (double)(dds.FindFirst(ImageProcessingTagHelper.PixelSpacing()).GetMultipleData()[1]);

        var dBS = (double)dds.FindFirst(ImageProcessingTagHelper.SpacingBetweenSlices()).GetData();

        var Tipp = new double[,] { { 1d, 0d, 0d, x }, { 0d, 1d, 0d, y }, { 0d, 0d, 1d, z }, { 0d, 0d, 0d, 1d } };
        //Debug.Log("Tipp e'");
        //DebugPrintMatrix44(Tipp);
        var S = new double[,] { { pixelSpacingY, 0d, 0d, 0d }, { 0d, pixelSpacingX, 0d, 0d }, { 0d, 0d, dBS, 0d }, { 0d, 0d, 0d, 1d } };
        //Debug.Log("S e'");
        //DebugPrintMatrix44(S);
        var TZero = new double[,] { { 1d, 0d, 0d, 0d }, { 0d, 1d, 0d, 0d }, { 0d, 0d, 1d, 0d }, { 0d, 0d, 0d, 1d } };
        //Debug.Log("Tzero e'");
        //DebugPrintMatrix44(TZero);
        var res = MatricesUtility.MultiplyMatrix(Tipp, MatricesUtility.MultiplyMatrix(R, MatricesUtility.MultiplyMatrix(S, TZero)));
        //Debug.Log("Mtf e'");
        //DebugPrintMatrix44(TZero);

        return res;
    }
}

public class AMatrix4x4
{
    private double[,] M;

    public AMatrix4x4(double[,] matrix)
    {
        this.M = matrix;
    }

    public double M00 { get { return M[0, 0]; } }
    public double M01 { get { return M[0, 1]; } }
    public double M02 { get { return M[0, 2]; } }
    public double M03 { get { return M[0, 3]; } }

    public double M10 { get { return M[1, 0]; } }
    public double M11 { get { return M[1, 1]; } }
    public double M12 { get { return M[1, 2]; } }
    public double M13 { get { return M[1, 3]; } }

    public double M20 { get { return M[2, 0]; } }
    public double M21 { get { return M[2, 1]; } }
    public double M22 { get { return M[2, 2]; } }
    public double M23 { get { return M[2, 3]; } }

    public double M30 { get { return M[3, 0]; } }
    public double M31 { get { return M[3, 1]; } }
    public double M32 { get { return M[3, 2]; } }
    public double M33 { get { return M[3, 3]; } }
}

public class Transformation3D
{
    //these 3 fields are used for applying the texture of the frame to a game object with:
    //- a sprite renderer
    //- a pivot point placed at its most top most-left coordinates
    public Vector3 Position { get; set; }
    public Vector3 Rotation { get; set; }
    public Vector3 Scale { get; set; }

    public Vector2 RowsColumns { get; set; }  //this vector2 is not strictly necessary for the frame to be displayed but maybe will be useful in future

    //these 2 fields are used for applying the texture of the frame to a game object with:
    //- a mesh renderer
    //- a pivot point placed at the centroid coordinates
    public Vector3 CentroidPosition { get; set; }
    public Vector3 CentroidScale { get; set; }

    public Transformation3D(Vector3 position, Vector3 rotation, Vector3 scale, Vector2 rowsColumns)
    {
        Position = position;
        Rotation = rotation;
        Scale = scale;
        RowsColumns = rowsColumns;
    }
}

public static class MatricesUtility
{
    public static Vector3 GetAngles2(AMatrix4x4 source)
    {
        double sy = Math.Sqrt(source.M00 * source.M00 + source.M10 * source.M10);

        bool singular = sy < 1e-6;

        double x, y, z;
        if (!singular)
        {
            x = Math.Atan2(-source.M21, source.M22);    //before it was: x = Math.Atan2(source.M21, source.M22);
            y = Math.Atan2(-source.M20, sy);
            z = Math.Atan2(source.M10, source.M00);
        }
        else
        {
            x = Math.Atan2(-source.M12, source.M11);
            y = Math.Atan2(-source.M20, sy);
            z = 0;
        }
        x += 1.57d;    //+90°(+1.57d) is required to convert from z-up(DICOM) to y-up(Unity)

        return new Vector3((float)x, (float)z, (float)y);
    }

    public static double[] Cross3D(double[] left, double[] right)
    {
        var result = new double[3];
        result[0] = left[1] * right[2] - left[2] * right[1];
        result[1] = -left[0] * right[2] + left[2] * right[0];
        result[2] = left[0] * right[1] - left[1] * right[0];
        return result;
    }

    public static Vector3 FromRadiansToDegrees(Vector3 radians)
    {
        double factor = 180 / Math.PI;
        return new Vector3((float)(radians.x * factor), (float)(radians.y * factor), (float)(radians.z * factor));
    }

    public static double[,] MultiplyMatrix(double[,] A, double[,] B)
    {
        int rA = A.GetLength(0);
        int cA = A.GetLength(1);
        int rB = B.GetLength(0);
        int cB = B.GetLength(1);
        double temp = 0;
        double[,] kHasil = new double[rA, cB];
        if (cA != rB)
        {
            Debug.Log("matrices can't be multiplied");
            return null;
        }
        else
        {
            for (int i = 0; i < rA; i++)
            {
                for (int j = 0; j < cB; j++)
                {
                    temp = 0;
                    for (int k = 0; k < cA; k++)
                    {
                        temp += A[i, k] * B[k, j];
                    }
                    kHasil[i, j] = temp;
                }
            }
            return kHasil;
        }
    }

    public static T[] GetRow<T>(T[,] matrix, int row)
    {
        var columns = matrix.GetLength(1);
        var array = new T[columns];
        for (int i = 0; i < columns; ++i)
            array[i] = matrix[row, i];
        return array;
    }

    public static T[] GetColumn<T>(T[,] matrix, int columnNumber)
    {
        return Enumerable.Range(0, matrix.GetLength(0))
                .Select(x => matrix[x, columnNumber])
                .ToArray();
    }
}