﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EvilDICOM.Core;
using EvilDICOM.Core.Interfaces;
using EvilDICOM.Core.Helpers;
using EvilDICOM.Core.Element;

/// <summary>
/// These 4 classes provides a wrapping so that thay are the only which have a dependency to the specific 
/// library used in the project(EvilDICOM). In addition, they provide a logical separation of the DICOM tags, 
/// filtering only the ones of interest for the application.
/// </summary>

public class DICOMDataset
{
    private DICOMObject dicomObj;

    public DICOMDataset(string filename)
    {
        dicomObj = DICOMObject.Read(filename);
    }

    public DICOMDataElement FindFirst(DICOMImageProcessingTag dth)
    {
        return new DICOMDataElement(dicomObj.FindFirst(dth.GetTag()));
    }
}

public class DICOMDataElement
{
    IDICOMElement myDe;
    public DICOMDataElement(IDICOMElement de)
    {
        myDe = de;
    }

    public object GetData()
    {
        return myDe.DData;   //return object
    }
    
    public IList GetMultipleData()
    {
        return myDe.DData_;
    }

    public bool isNull()
    {
        return myDe == null;
    }
}

public class ImageProcessingTagHelper
{
    //pixel extracting tags
    public static DICOMImageProcessingTag PhotometricInterpretation()
    {
        return new DICOMImageProcessingTag(TagHelper.Photometric​Interpretation);
    }
    public static DICOMImageProcessingTag BitsAllocated()
    {
        return new DICOMImageProcessingTag(TagHelper.BitsAllocated);
    }
    public static DICOMImageProcessingTag HighBit()
    {
        return new DICOMImageProcessingTag(TagHelper.HighBit);
    }
    public static DICOMImageProcessingTag BitsStored()
    {
        return new DICOMImageProcessingTag(TagHelper.BitsStored);
    }
    public static DICOMImageProcessingTag RescaleIntercept()
    {
        return new DICOMImageProcessingTag(TagHelper.RescaleIntercept);
    }
    public static DICOMImageProcessingTag RescaleSlope()
    {
        return new DICOMImageProcessingTag(TagHelper.RescaleSlope);
    }
    public static DICOMImageProcessingTag Rows()
    {
        return new DICOMImageProcessingTag(TagHelper.Rows);
    }
    public static DICOMImageProcessingTag Columns()
    {
        return new DICOMImageProcessingTag(TagHelper.Columns);
    }
    public static DICOMImageProcessingTag PixelRepresentation()
    {
        return new DICOMImageProcessingTag(TagHelper.PixelRepresentation);
    }
    public static DICOMImageProcessingTag PixelData()
    {
        return new DICOMImageProcessingTag(TagHelper.PixelData);
    }
    public static DICOMImageProcessingTag WindowWidth()
    {
        return new DICOMImageProcessingTag(TagHelper.WindowWidth);
    }
    public static DICOMImageProcessingTag WindowCenter()
    {
        return new DICOMImageProcessingTag(TagHelper.WindowCenter);
    }

    //spatial tags
    public static DICOMImageProcessingTag PixelSpacing()
    {
        return new DICOMImageProcessingTag(TagHelper.PixelSpacing);
    }
    public static DICOMImageProcessingTag ImagePositionPatient()
    {
        return new DICOMImageProcessingTag(TagHelper.ImagePositionPatient);
    }
    public static DICOMImageProcessingTag ImageOrientation​Patient()
    {
        return new DICOMImageProcessingTag(TagHelper.ImageOrientationPatient);
    }
    public static DICOMImageProcessingTag SpacingBetweenSlices()
    {
        return new DICOMImageProcessingTag(TagHelper.SpacingBetweenSlices);
    }
}

public class DICOMImageProcessingTag
{
    Tag tg;
    public DICOMImageProcessingTag(Tag tag)
    {
        tg = tag;
    }
   
    public Tag GetTag()
    {
        return this.tg;
    }
}