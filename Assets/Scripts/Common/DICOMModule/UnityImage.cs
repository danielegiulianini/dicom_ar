﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class models an image in the world of Unity, with a method to traslate it to a 2D texture.
/// </summary>
public abstract class UnityImage<T> : AImage<T>
{
    public UnityImage(int width, int height) : base(width, height)
    {
    }

    public UnityImage(T[] pixelArray, int width, int height) : base(pixelArray, width, height)
    {
    }

    public abstract Texture2D ConvertToTexture();
}
