﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// A Loader for DICOM Frames.
/// </summary>
public interface IFrameLoader
{
    void LoadFrames();  //no need modifiers as they all public by def in interface

    IList<DICOMFrame> GetFrames();
}
