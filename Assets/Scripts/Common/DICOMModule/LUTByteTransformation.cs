﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provided a mapping between input pixels and output, this class performs a transformation from ushort to 
/// byte by the means of a LUT for efficiency reasons. 
/// </summary>
public class LUTByteTransformation : IImageTransformation<byte>
{
    private AImage<ushort> input;
    private AImage<byte> lut;
    private AImage<byte> output;

    //I should implement the constructor with the LUT already made, extracted from DICOM

    public LUTByteTransformation(AImage<ushort> input, PixelFunction<ushort, byte> pf)
    {
        this.input = input; //deep copy of input image? not necessary as I don't edit it.
        lut = new AImage<byte>(32768, 2);
        output = new AImage<byte>(input.Width, input.Height);
        for (ushort i = 0; i < 65535; i++)
        {
            byte ris = pf(i);
            lut[i] = ris > 255 ? (byte)255 : ris;
        }
    }

    public AImage<byte> Apply()
    {
        for (int i = 0; i < output.Width * output.Height; i++)
        {
            output[i] = lut[input[i]];
        }
        return output;
    }
}
