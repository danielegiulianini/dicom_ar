﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AImage<T>
{
    protected T[] imageArray;

    public AImage(int width, int height)
    {
        imageArray = new T[width*height];
        Width = width;
        Height = height;
    }

    public AImage(T[] pixelArray, int width, int height)
    {
        imageArray = PerformDeepCopy(pixelArray);

        Width = width;
        Height = height;
    }

    protected T[] PerformDeepCopy(T[] aArray)  //perform a deep copy (otherwise would keep the reference to the same...)
    {
        T[] deepCopy = new T[aArray.Length];
        for (int i = 0; i < deepCopy.Length; i++)
        {
            deepCopy[i] = aArray[i];
        }
        return deepCopy;
    }

    public T[] AsArray()
    {
        return PerformDeepCopy(imageArray);
    }

    public T this[int index]
    {
        /*should add checking*/
        get
        {
            return imageArray[index];
        }

        set
        {
            imageArray[index] = value;
        }
    }

    public int Width
    {
        get; private set;
    }

    public int Height
    {
        get; private set;
    }
}
