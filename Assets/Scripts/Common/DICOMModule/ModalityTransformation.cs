﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class models the modality transformation as described at the part 3 of the standard.
/// </summary>
public class ModalityTransformation : IImageTransformation<ushort>
{
    //properties could be used to prevent creating new objects every time these 2 files change
    private GreyscaleParameters gp;
    private AImage<ushort> input;

    public ModalityTransformation(GreyscaleParameters gp, AImage<ushort> input) {
        this.gp = gp;
        this.input = input;
    }

    public AImage<ushort> Apply()
    {
        return new LUTIntTrasformation(input, valgray => (ushort)(gp.Slope * valgray + gp.Intercept)).Apply();
    }
}
