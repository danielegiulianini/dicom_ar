﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DICOMGreyscaleImage : UnityImage<byte>
{

    public DICOMGreyscaleImage(AImage<byte> img, int width, int height) : base (img.AsArray(), width, height)
    {
    }

    public DICOMGreyscaleImage(byte[] imgarray, int width, int height) : base(imgarray, width, height)
    {
    }

    public override Texture2D ConvertToTexture()
    {
        Texture2D tex;
        Color[] colors = new Color[Width * Height];
        for (int i = 0; i < Width * Height; i++)
        {
            float inv = 1f / 255.0f;    //normalized because SetPixels wants it in this way (0...1)
            int v = (int)this[i];
            float f = inv * v;
            colors[i] = new Color(f, f, f, 1);
        }
        tex = new Texture2D(Width, Height, TextureFormat.ARGB32, false);
        tex.SetPixels(colors);
        tex.Apply();
        return tex;
    }
}
