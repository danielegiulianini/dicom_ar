﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A loader for non-acquisition frames, that are the images that are't directly supplied by the acquisition
/// devices (for this purpose see AcquisitionFramesLoader).
/// </summary>
public class NonAcquisitionFramesLoader
{
}
