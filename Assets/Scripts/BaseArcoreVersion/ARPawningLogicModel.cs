﻿using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Controls the instantiation of the 3D model.
/// </summary>
public class ARPawningLogicModel : ARPawningLogic
{
    private bool instantiated = false;

    //public as will be assigned from inspector
    public GameObject segmentedModelPrefab;

    /// <summary>
    /// The rotation in degrees need to apply to prefab when it is placed.
    /// </summary>
    private const float k_PrefabRotation = 180.0f;

    /// <summary>
    /// The Unity Awake() method.
    /// </summary>
    public override void Awake()
    {
        //Debug.Log("from inside awake");
    }

    /// <summary>
    /// The Unity Update() method.
    /// </summary>
    public override void Update()
    {
        //Debug.Log("dicomAr, ecco la persistent path");
        Debug.Log(Application.persistentDataPath);

        // If the player has not touched the screen, we are done with this update.
        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }

        // Should not handle input if the player is pointing on UI.
        if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        {
            return;
        }

        // Raycast against the location the player touched to search for planes.
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
        TrackableHitFlags.FeaturePointWithSurfaceNormal;
        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            // Use hit pose and camera pose to check if hittest is from the
            // back of the plane, if it is, no need to create the anchor.
            if ((hit.Trackable is DetectedPlane) &&
                Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else if (hit.Trackable is DetectedPlane)
            {
                if (!instantiated)
                {
                    // Instantiate prefab at the hit pose.
                    var segmentedModelInstance = Instantiate(segmentedModelPrefab, hit.Pose.position, hit.Pose.rotation);

                    segmentedModelInstance.transform.Rotate(0, k_PrefabRotation, 0, Space.Self);// Compensate for the hitPose rotation facing away from the raycast (i.e. camera).

                    // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical world evolves.
                    var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                    segmentedModelInstance.transform.parent = anchor.transform;

                    instantiated = true;

                    DisablePlaneDetection();
                }
            }
        }
    }
}
