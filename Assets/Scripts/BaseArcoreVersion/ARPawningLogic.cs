﻿using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Contains common code for instantiating objects with ARCore.
/// </summary>
public abstract class ARPawningLogic : MonoBehaviour
{
    /// <summary>
    /// The first-person camera being used to render the passthrough camera image (i.e. AR
    /// background).
    /// </summary>
    public Camera FirstPersonCamera;

    public abstract void Awake();

    // Update is called once per frame
    public abstract void Update();

    protected void DisablePlaneDetection()
    {
        //these tags must be added before running the code
        foreach (GameObject plane in GameObject.FindGameObjectsWithTag("plane"))
        {
            Renderer r = plane.GetComponent<Renderer>();
            DetectedPlaneVisualizer t = plane.GetComponent<DetectedPlaneVisualizer>();
            r.enabled = false;
            t.enabled = false;
        }

        foreach (GameObject obj2 in GameObject.FindGameObjectsWithTag("planeDiscovery"))
        {
            obj2.SetActive(false);
        }

        foreach (GameObject obj2 in GameObject.FindGameObjectsWithTag("planeGenerator"))
        {
            obj2.SetActive(false);
        }

        foreach (GameObject point in GameObject.FindGameObjectsWithTag("point"))
        {
            Renderer r = point.GetComponent<Renderer>();
            PointcloudVisualizer t = point.GetComponent<PointcloudVisualizer>();
            r.enabled = false;
            t.enabled = false;
        }

        var obj3 = GameObject.Find("Point Cloud");
        obj3.SetActive(false);
    }
}
