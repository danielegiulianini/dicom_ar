﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

/// <summary>
/// This script handles the logic of displaying the DICOM frame chosen by the user by the means of a slider.
/// </summary>
public class SimpleFramesController : MonoBehaviour
{
    public Slider indexSlider;  //this is the gameobject, not the script
    public Vector3 pose { get; set; }
    private IList<DICOMFrame> frames;

    void Start()
    {
        //Debug.Log("sfc start");
        var DICOMDIRPath = Path.Combine(Application.persistentDataPath, "DICOM"); ;   //var DICOMDIRPath = Application.dataPath + "\\Resources\\DICOM";
        Debug.Log(DICOMDIRPath);
        var myFl = new NonOrientedFramesLoader(DICOMDIRPath, new Vector3(pose.x, pose.y, pose.z+0.5f)); 
        myFl.LoadFrames();
        frames = myFl.GetFrames();
        //Debug.Log("sfc count: "+frames.Count);
        DisplayFrame(frames[0]);

        indexSlider.GetComponent<Slider>().maxValue = 0;    //setto il min e max del slider
        indexSlider.GetComponent<Slider>().maxValue = frames.Count - 1;
        indexSlider.onValueChanged.AddListener( //indexSlider.onValueChanged += () => { fd.DisplayFrame(frames[mainSlider.value]); };  //registrazione all'evento
             delegate { ValueUpdate(); }
        );
    }

    private void ValueUpdate()
    {
        UpdateFrame(frames[(int)indexSlider.value]);
    }

    private void DisplayFrame(DICOMFrame df)
    {
        UpdateFrame(df);
        transform.localScale = new Vector3(df.Transformation.Scale.x * df.Sprite.pixelsPerUnit, df.Transformation.Scale.y * df.Sprite.pixelsPerUnit, 1);
    }

    private void UpdateFrame(DICOMFrame df)
    {
        GetComponent<SpriteRenderer>().sprite = df.Sprite;
    }
}
