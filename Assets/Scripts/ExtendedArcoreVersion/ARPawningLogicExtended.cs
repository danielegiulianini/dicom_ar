﻿using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Controls the instantiation of the 3D Model and the frames controller responsible of 
/// managing the intersection between DICOM frames and the 3d Model.
/// </summary>
public class ARPawningLogicExtended : ARPawningLogic
{
    private GameObject slider;
    private bool instantiated = false;

    //public as will be assigned from inspector
    public GameObject framesContainerPrefab;
    public GameObject segmentedModelPrefab;

    /// <summary>
    /// The rotation in degrees need to apply to prefab when it is placed.
    /// </summary>
    private const float k_PrefabRotation = 180.0f;

    /// <summary>
    /// The Unity Awake() method.
    /// </summary>
    public override void Awake()
    {
        slider = GameObject.Find("Slider");
        slider.SetActive(false);    //slider should be disabled until frames are displayed (in scene is disabled)
    }

    /// <summary>
    /// The Unity Update() method.
    /// </summary>
    public override void Update()
    {
        Debug.Log(Application.persistentDataPath + "/DICOM");

        // If the player has not touched the screen, we are done with this update.
        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }

        // Should not handle input if the player is pointing on UI.
        if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        {
            return;
        }

        // Raycast against the location the player touched to search for planes.
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
        TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            // Use hit pose and camera pose to check if hittest is from the
            // back of the plane, if it is, no need to create the anchor.
            if ((hit.Trackable is DetectedPlane) &&
                Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else if (hit.Trackable is DetectedPlane)
            {
                if (!instantiated)
                {
                    var framesContainerInstance = Instantiate(framesContainerPrefab, hit.Pose.position, hit.Pose.rotation);
                    
                    // Instantiate prefab at the hit pose.
                    var segmentedModelInstance = Instantiate(segmentedModelPrefab, hit.Pose.position, hit.Pose.rotation);

                    //Make slider visible
                    slider.SetActive(true);

                    //set the fiels of the ClippingModelFramesController instance
                    framesContainerInstance.GetComponent<ClippingModelFramesController>().indexSlider = slider.GetComponent<Slider>();
                    framesContainerInstance.GetComponent<ClippingModelFramesController>().segmentedModel = segmentedModelInstance;

                    segmentedModelInstance.transform.Rotate(0, 180, 0, Space.Self);// Compensate for the hitPose rotation facing away from the raycast (i.e. camera).

                    //eventually add lean touch for manipulation (or add the lean touch directly to the scripts of the prefab to spawn)

                    //Create an anchor to allow ARCore to track the hitpoint as understanding of the physical world evolves.
                    var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                    // Make game object a child of the anchor.
                    //framesContainerInstance.transform.parent = anchor.transform;    //should remove as framesController checks for the model position at every frame
                    segmentedModelInstance.transform.parent = anchor.transform;

                    DisablePlaneDetection();
                    instantiated = true;
                }
            }
        }
    }
}
