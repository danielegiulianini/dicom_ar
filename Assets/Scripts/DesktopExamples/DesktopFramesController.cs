﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;


/// <summary>
/// This script, attached to a gameobject like in the FramesSpritesControllerScene, manages the intersection
/// of DICOM acquisition frames with 3d model. That scene is only an example and it is provided to see 
/// (from inside the Unity editor) how to load DICOM frames and how intersection works without having to 
/// deploy the application to a specific device (like Android tablet or headset).
/// So, when entering playing mode, this scene must be viewed from the scene view and you can try how it 
/// works by changing the slider value from the inspector.
/// 
/// In this example the functionality of cuttig the mesh isn't provided, see ClippingModelFramesController
/// script and related scene for it.
/// </summary>
public class DesktopFramesController : MonoBehaviour
{
    public Slider indexSlider;  //this is the gameobject, not the script
    public GameObject segmentedModel;
    private IList<DICOMFrame> frames;
  
    void Start()
    {
        var DICOMDIRPath = Application.dataPath + "\\Resources\\DICOM"; //Debug.Log(DICOMDIRPath);

        var myFl = new OrientedFramesLoader(DICOMDIRPath);
        myFl.LoadFrames();
        frames = myFl.GetFrames();
        DisplayFrame(frames[0]);

        indexSlider.GetComponent<Slider>().maxValue = 0;    //setto il min e max del slider
        indexSlider.GetComponent<Slider>().maxValue = frames.Count - 1;
        indexSlider.onValueChanged.AddListener( //indexSlider.onValueChanged += () => { fd.DisplayFrame(frames[mainSlider.value]); };  //registrazione all'evento
             delegate { ValueUpdate(); }
        );
    }

    private void ValueUpdate()
    {
        DisplayFrame(frames[(int)indexSlider.value]);
    }

    private void DisplayFrame(DICOMFrame df)
    {
        GetComponent<SpriteRenderer>().sprite = df.Sprite;
        transform.position = df.Transformation.Position;
        transform.rotation = Quaternion.Euler(df.Transformation.Rotation);
        transform.localScale = new Vector3(df.Transformation.Scale.x * df.Sprite.pixelsPerUnit, df.Transformation.Scale.y * df.Sprite.pixelsPerUnit, 1);
    }

    //update method is only required for a debugging purpose as the this code is executed from ValueUpdate,
    //its purpose is to change the slider value from inside the scene view, otherwise it doesn't work
    void Update()
    {
        DisplayFrame(frames[(int)indexSlider.value]); 
    }
}
