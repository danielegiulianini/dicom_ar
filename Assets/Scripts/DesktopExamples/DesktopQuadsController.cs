﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

/// <summary>
/// This script is the equivalent of DesktopFramesController, it handles the intersection between DICOM 
/// frames and the 3D Model, but instead of gameobject with spriterenderer, it's attached to game objects 
/// with a meshrenderer (in particular: quads). Sprite renderer has some problems of transparency in Meta 2, 
/// mesh renderer not. See SpatialInfoExtractor class for more info about it.
/// 
/// That scene must be viewed from scene view and you can try how it works by changing the slider value 
/// from the inspector.
/// In this example the functionality of cuttig the mesh isn't provided, see ClippingModelFramesController 
/// and related scene for it.
/// </summary>

public class DesktopQuadsController : MonoBehaviour
{
    public Slider indexSlider;  //this is the gameobject, not the script
    public Vector3 pose { get; set; }
    public GameObject segmentedModel;
    private IList<DICOMFrame> frames;

    void Start()
    {
        var DICOMDIRPath = Application.dataPath + "\\Resources\\DICOM";
        Debug.Log(DICOMDIRPath);

        var myFl = new OrientedFramesLoader(DICOMDIRPath);
        myFl.LoadFrames();
        frames = myFl.GetFrames();
        DisplayFrame(frames[0]);

        indexSlider.GetComponent<Slider>().maxValue = 0;    //setto il min e max del slider
        indexSlider.GetComponent<Slider>().maxValue = frames.Count - 1;
        indexSlider.onValueChanged.AddListener( //indexSlider.onValueChanged += () => { fd.DisplayFrame(frames[mainSlider.value]); };  //registrazione all'evento
             delegate { ValueUpdate(); }
        );
    }

    private void ValueUpdate()
    {
        DisplayFrame(frames[(int)indexSlider.value]);
    }

    private void DisplayFrame(DICOMFrame df)
    {
        GetComponent<Renderer>().material.mainTexture = df.AsTexture;
        transform.position = df.Transformation.CentroidPosition;
        transform.rotation = Quaternion.Euler(df.Transformation.Rotation);
        transform.localScale = df.Transformation.CentroidScale;
    }

    //update method is only required for a debugging purpose when running inside the Unity editor as the 
    //this code is executed from ValueUpdate, its purpose is to change the slider value from inside the 
    //scene view, otherwise it doesn't work
    void Update()
    {
        DisplayFrame(frames[(int)indexSlider.value]); 
    }
}
