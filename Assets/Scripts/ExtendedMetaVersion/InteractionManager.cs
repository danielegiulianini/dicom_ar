﻿using Meta;
using Meta.HandInput;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Script responsible for the handling user input in meta. It's inspired by the class MetaInteraction of 
/// Meta 2 SDK, but provides an abstract class that can be implemented by other classes, otherwise not possible.
/// 
/// THIS SCRIPT, THE SCRIPT MetaFramesController AND THE SCENE MetaExtendedVersion HAVE NOT BEEN TESTED, 
/// THOSE ARE ONLY A HINT FOR FUTURE DEVELOPMENT.
/// 
/// </summary>
public abstract class InteractionManager : Interaction
{
    private HandFeature _handFeature;
    private Vector3 _localOffset;

    protected override bool CanEngage(Hand handProxy)
    {
        return GrabbingHands.Count == 1;
    }

    protected override void Engage()
    {
        _handFeature = GrabbingHands[0];
        PrepareRigidbodyForInteraction();
        // Store the offset of the object local to the hand feature.  This will be used to keep the object at the same distance from the hand when being moved.
        SetHandOffsets();
    }

    protected override bool CanDisengage(Hand handProxy)
    {
        if (_handFeature != null && handProxy.Palm == _handFeature)
        {
            foreach (var hand in GrabbingHands)
            {
                if (hand != _handFeature)
                {
                    _handFeature = hand;
                    SetHandOffsets();
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    protected override void Disengage()
    {
        Manipulate();
        OnDisengage();

        RestoreRigidbodySettingsAfterInteraction();
        _handFeature = null;
    }

    protected abstract void OnDisengage();

    protected Vector3 TransformedHandPos()
    {
        // This complements obtaining the offset - used to convert back to world space.
        Vector3 offset = _handFeature.transform.TransformDirection(_localOffset);
        Vector3 grabPosition = _handFeature.transform.position + offset;
        return grabPosition;
    }

    private void SetHandOffsets()
    {
        _localOffset = _handFeature.transform.InverseTransformDirection(TargetTransform.position - _handFeature.Position);
        SetGrabOffset(TransformedHandPos());
    }
}
