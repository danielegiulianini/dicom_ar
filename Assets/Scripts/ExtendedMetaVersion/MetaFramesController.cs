﻿using Meta;
using Meta.HandInput;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Script responsible for the intersection of the slice to the 3d model in Meta. It translates the movement
/// of the user hand into the index of the slice to be displayed and translates the slices along the vertical
/// axes to its position in the patient coordinates system. 
/// 
/// THIS SCRIPT, THE SCRIPT InteractionManager AND THE SCENE MetaExtendedVersion HAVE NOT BEEN TESTED, 
/// THAT ARE ONLY A HINT FOR FUTURE DEVELOPMENT.
/// 
/// </summary>
public class MetaFramesController : InteractionManager
{
    private Vector3 startingPos;
    private bool ended = true;
    private IList<DICOMFrame> frames;
    private int currentSliceIndex;

    public void Start()
    {
        var DICOMDIRPath = Application.dataPath + "\\Resources\\DICOM";
        Debug.Log(DICOMDIRPath);

        var myFl = new OrientedFramesLoader(DICOMDIRPath);
        myFl.LoadFrames();
        frames = myFl.GetFrames();
        currentSliceIndex = 0;
        DisplayFrame(frames[currentSliceIndex]);
    }

    private void DisplayFrame(DICOMFrame df)
    {
        GetComponent<Renderer>().material.mainTexture = df.AsTexture;
        transform.position = df.Transformation.CentroidPosition;
        transform.rotation = Quaternion.Euler(df.Transformation.Rotation);
        transform.localScale = df.Transformation.CentroidScale;
    }

    protected override void Manipulate()
    {
        if (ended == true)  //at the beginning of manipulation set starting position
        {
            ended = false;
            startingPos = TransformedHandPos();
        }

        DisplayFrame(frames[ComputeIndex(startingPos, TransformedHandPos())]);
        Move(TransformedHandPos());
    }

    protected override void OnDisengage()
    {
        ended = true;
    }

    private int ComputeIndex(Vector3 startingPos, Vector3 currentPos)
    {
        var verticalDistance = currentPos.y - startingPos.y;
        var overallDistance = 0.5;
        var indexOffset = verticalDistance / overallDistance * frames.Count;
        var newSliceIndex = currentSliceIndex + indexOffset;
        currentSliceIndex = newSliceIndex.ClipToZero().ClipToMax(frames.Count);
        return currentSliceIndex;
    }
}

public static class MyExtensions
{
    public static int ClipToZero(this double value)
    {
        return value < 0f ? 0 : (int)value;
    }
    public static int ClipToMax(this int value, int max)
    {
        return (int)value > max ? max : (int)value;
    }
}
