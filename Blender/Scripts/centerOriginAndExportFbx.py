import bpy
import sys

pathToObjToImport = 'C:\\Users\\Patrizia\\Desktop\\3dModelStuff\\objWay\\heart_single_noVq.obj'
pathToFbxToExport = 'D:\\exported.fbx'
scalingFactorX = 1
scalingFactorY = 1
scalingFactorZ = 1

#delete all objects
#for item in bpy.context.scene.objects:
for item in bpy.data.objects:
    bpy.data.objects[item].select = True
    item.select_set(True)
bpy.ops.object.delete()

#import obj
imported_object = bpy.ops.import_scene.obj(filepath=pathToObjToImport)

#get the obj for processing it
obj_object = bpy.context.selected_objects[0]
print('Imported name: ', obj_object.name)

#rescale the obj
obj_object.scale = (scalingFactorX , scalingFactorY , scalingFactorZ )

def CenterOrigin():
    #Get active object    
    act_obj=obj_object	#act_obj = bpy.context.active_object
        
    #Get cursor
    cursor = bpy.context.scene.cursor

    #Get original cursor location
    original_cursor_location = (cursor.location[0], cursor.location[1], cursor.location[2])   

    #Make sure origin is set to geometry for cursor z move 
    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')  

    #Set cursor location to object location
    cursor.location = act_obj.location

    #Get cursor z move  
    half_act_obj_z_dim = act_obj.dimensions[2] / 2
    cursor_z_move = cursor.location[2] - half_act_obj_z_dim   

    #Move cursor to bottom of object
    cursor.location[2] = cursor_z_move

    #Set origin to cursor
    bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
    
    #Reset cursor back to original location
    cursor.location = original_cursor_location

    #Assuming you're wanting object center to grid
    bpy.ops.object.location_clear(clear_delta=False)
                
CenterOrigin()

#export fbx
bpy.ops.export_scene.fbx(filepath=pathToFbxToExport, axis_forward='-Z', axis_up='Y')	#, use_selection = True